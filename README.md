# Ballot Box Smart Contract POC

Ballot-Box Smart Contracts implement secure and transparent collection, verification, aggregation and tally of ballots for a specific vote event, as well as the publication of the vote's results. 